unit
    lang;

{$mode objfpc}{$H+}
interface

uses
    Classes, SysUtils, XMLRead, XMLWrite, DOM, Dialogs;

function makeLangFilename( fn : string ) : string;
function checkLangFileExists( fn : string ) : Boolean;
function loadLangFile( fn : string ) : Boolean;
function getLangEntry( key : string ) : string;

implementation

var
    langFileName : string;

    langEntries : TStringList;

    function makeLangFilename( fn : string ) : string;
    begin
        langFileName := 'lang/' + fn + '.xml';
        makeLangFilename := langFileName;
    end;

    function checkLangFileExists( fn : string ) : Boolean;
    Var
        F : File;

    begin
        {$i-}
        Assign(F, fn);
        Reset(F);
        {$I+}
        checkLangFileExists := (IoResult = 0) and (fn <> '');
        Close(F);
    end;

    function loadLangFile( fn : string ) : Boolean;
    Var
        Doc : TXMLDocument;
        Child : TDOMNode;
        val : TDOMNode;
        key : string;
        value : string;

    begin
        langEntries := TStringList.Create;

        try
            ReadXMLFile(Doc, fn);
        except
            begin
                ShowMessage('Unable to load specified language file: ' + fn + ', file is missing or damaged');
                halt();
            end;
        end;
        Child := Doc.DocumentElement.FirstChild;

        // iterate through the entries
        while Assigned(Child)do
            begin
                key := Child.firstChild.TextContent;
                val := Child.firstChild.NextSibling;
                value := val.TextContent;
                langEntries.add(key + '=' + value);
                Child := Child.NextSibling;
            end;
        loadLangFile := true;
    end;

    function getLangEntry( key : string ) : string;
    begin
        getLangEntry := langEntries.Values[key];
    end;
end.