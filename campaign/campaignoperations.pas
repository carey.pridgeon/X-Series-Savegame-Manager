unit
    campaignOperations;

{$mode objfpc}{$H+}
interface

uses
    Classes, SysUtils, FileUtil, md5, XMLRead, XMLWrite, DOM, Dialogs, shlobj, StdCtrls,ExtCtrls, ComCtrls,math;

type
    TActiveGame = ( X3, X3TC, X3AP, XR );

function copyFile( source : string; dest : string ) : boolean;

function delete( target : string ) : boolean;

function deleteFolderContent( targetDir : string ) : boolean;

function CopyCampaign( sourceDir : string; destDir : string; ProgressBar :TProgressBar ) : boolean;

function moveFile( source : string; dest : string ) : integer;

function GetMyDocuments : string;

function DoesDirectoryExist( const directory : string ) : boolean;

procedure GetSubDirectories( const directory : string; list : TStrings );

procedure GetDirectoryContents( const Path : String; Attr : Integer; List : TStrings );

function IsDirectoryEmpty( const directory : string ) : boolean;

function deleteDirectory( const directory : string ) : boolean;

function getCampaignName( const cfg : string ) : string;

function getCampaignVersion( const cfg : string ) : string;

function getLogEntryTemplate( const cfg : string ) : string;

function readLogFile( const cfg : string ) : TStringList;

function GetFileName( FullPath : string ) : string;

function campaignFileExists( cfg : string ) : boolean;

function makeCampaignFile( name : string; description : string; dest : string ) : boolean;

function writeLogFile( const cfg : string; const newEntry : string ) : boolean;

implementation

function copyFile( source : string; dest : string ) : boolean;
var
    SourceName, DestName : string;

    SourceF, DestF : file;

    fileHash, newHash : string;

    Block : array [ 0..1023 ] of Byte;

    NumRead : Integer;

    fa : Longint;
//Var S : TDateTime;

begin
    if FileExists(source)then
        begin
            NumRead := 0;
            fa := FileAge(source);
            //S:=FileDateTodateTime(fa);
            //ShowMessage ('I''m from '+DateTimeToStr(S));
            AssignFile(SourceF, source);
            AssignFile(DestF, dest);
            // get the md5 of the original file
            fileHash := MD5Print(MD5File(source));
            FileMode := 0;     // open for read only
            Reset(SourceF, 1); // open source file
            Rewrite(DestF, 1); // Create destination file

            while not Eof(SourceF)do
                begin
                    // Read Byte from source file
                    BlockRead(SourceF, Block, SizeOf(Block), NumRead);
                    // Write this byte into new destination file
                    BlockWrite(DestF, Block, NumRead);
                end;
            CloseFile(SourceF);

            CloseFile(DestF);
            FileSetDate(dest, fa);
            newHash := MD5Print(MD5File(dest));

            if not ( AnsiCompareStr(fileHash, newHash) = 0 ) then
                begin
                    copyFile := false;
                end
        end
    else // Source File not found
        copyFile := false;
    copyFile := true;
end;

function delete( target : string ) : boolean;
begin
    delete := DeleteFile(target);
end;

function deleteFolderContent( targetDir : string ) : boolean;
var
    x : integer;

    contents : TStringlist;

    check : boolean;

begin
    contents := TStringlist.create;
    check := true;
    GetDirectoryContents(targetDir, faAnyFile, contents);

    for x := 0 to contents.count - 1 do
        begin
            if ( CompareStr(GetFileName(contents[x]),'.') <> 0 ) and (CompareStr(GetFileName(contents[x]), '..') <> 0) then
                begin
                  check := delete(targetDir + '\' + GetFileName(contents[x]));

                  if check = false then
                     break;
                end;
        end;

    if check = false then
        Result := false
    else
        Result := true;
end;

function moveFile( source : string; dest : string ) : integer;
var
    output : boolean;

begin
    If FileExists(source)Then
        begin
            output := copyFile(source, dest);

            if not output then
                moveFile := 1;
            output := DeleteFile(source);

            if not output then
                moveFile := 2;
            moveFile := 0;
        end
    else
        moveFile := 3;
end;

function CopyCampaign( sourceDir : string; destDir : string; ProgressBar :TProgressBar) : boolean;
var
    x : integer;

    contents : TStringlist;

    check : boolean;
    pos : integer;
    up : integer;
begin
    contents := TStringlist.create;
    check := false;
    GetDirectoryContents(sourceDir, faAnyFile, contents);
    up := 0;
    pos := 0;
    ProgressBar.position := 0;
     up := floor(100 div contents.count);
    for x := 0 to contents.count - 1 do
        begin
            if ( CompareStr(GetFileName(contents[x]),
                '.') <> 0 ) and (CompareStr(GetFileName(contents[x]), '..') <> 0) then
                begin
                    //showmessage(sourceDir +'\'+contents[x]);
                    //showmessage(destdir+'\'+GetFileName(contents[x]));
                    check := CopyFile(sourceDir + '\' + contents[x], destdir + '\' + GetFileName(contents[x]));
                    progressBar.Caption:= destDir +' ' +contents[x];
                    inc(pos,up);
                    ProgressBar.position := pos;
                    if check = false then
                        break;
                end;
        end;
     ProgressBar.position := 0;
    if check = false then
        Result := false
    else
        Result := true;
end;

function GetMyDocuments : string;
var
    r : Boolean;

    path : array [ 0..Max_Path ] of Char;

begin
    r := ShGetSpecialFolderPath(0, path, CSIDL_Personal, False);

    if not r then
        begin
            showmessage('Could not find Documents folder location.');
            Result := 'error';
        end;
    Result := Path;
end;

procedure GetSubDirectories( const directory : string; list : TStrings );
var
    sr : TSearchRec;

begin
    try
        if FindFirst(IncludeTrailingPathDelimiter(directory) + '*.*', faDirectory, sr) < 0 then
            Exit
        else
            repeat
                if ( ( sr.Attr and faDirectory <> 0 ) AND (sr.Name <> '.') AND (sr.Name <> '..') ) then
                    List.Add(IncludeTrailingPathDelimiter(directory) + sr.Name);
            until FindNext(sr) <> 0;
    finally
        SysUtils.FindClose(sr);
    end;
end;

procedure GetDirectoryContents( const Path : String; Attr : Integer; List : TStrings );
var
    Res : TSearchRec;

    EOFound : Boolean;

begin
    EOFound := False;

    if FindFirst(Path + '\*.*', Attr, Res) < 0 then
        exit
    else
        while not EOFound do
            begin
                List.Add(Res.Name);
                EOFound := FindNext(Res) <> 0;
            end;
    FindClose(Res);
end;

//returns true if a given directory is empty, false otherwise
function IsDirectoryEmpty( const directory : string ) : boolean;
var
    searchRec : TSearchRec;

begin
    try
        result := (FindFirst(directory + '\*.*', faAnyFile,
            searchRec) = 0) AND (FindNext(searchRec) = 0) AND (FindNext(searchRec) <> 0);
    finally
        FindClose(searchRec);
    end;
end;

//returns true if a given directory exists, false otherwise
function DoesDirectoryExist( const directory : string ) : boolean;
begin
    if DirectoryExists(directory)then
        DoesDirectoryExist := true
    else
        DoesDirectoryExist := false;
end;

function deleteDirectory( const directory : string ) : boolean;
begin
    if not RemoveDir(directory)then
        deleteDirectory := false;
    deleteDirectory := true;
end;

function getCampaignName( const cfg : string ) : string;
var
    cNameNode : TDOMNode;

    Doc : TXMLDocument;

const
    campaignFile = '\campaign file.xml';

begin
    try
        // Read in xml file from disk
        ReadXMLFile(Doc, cfg + campaignFile);
        // Retrieve the "campaignName" node
        cNameNode := Doc.DocumentElement.FindNode('campaignName');
        //getCampaignName := cNameNode.FirstChild.NodeValue;
        getCampaignName := cNameNode.FirstChild.TextContent;
    finally
        // finally, free the document
        Doc.Free;
    end;
end;

function getCampaignVersion( const cfg : string ) : string;
var
    vNode : TDOMNode;

    Doc : TXMLDocument;

const
    campaignFile = '\campaign file.xml';

begin
    try
        // Read in xml file from disk
        ReadXMLFile(Doc, cfg + campaignFile);
        // Retrieve the "campaignName" node
        vNode := Doc.DocumentElement.FindNode('SGMversion');
        getCampaignVersion := vNode.FirstChild.TextContent;
    finally
        // finally, free the document
        Doc.Free;
    end;
end;

function getLogEntryTemplate( const cfg : string ) : string;
var
    ltNode : TDOMNode;

    Doc : TXMLDocument;

    s : string;

const
    campaignFile = '\campaign file.xml';

begin
    try
        // Read in xml file from disk
        ReadXMLFile(Doc, cfg + campaignFile);
        // Retrieve the "campaignName" node
        ltNode := Doc.DocumentElement.FindNode('logEntryTemplate');
        s := ltNode.FirstChild.TextContent;
        s := StringReplace(s, #10, '^', [ rfReplaceAll ]);
        s := s + '^';
        //showmessage(s);
        getLogEntryTemplate := s;
    finally
        // finally, free the document
        Doc.Free;
    end;
end;

function readLogFile( const cfg : string ) : TStringList;
var
    Doc : TXMLDocument;

    Child : TDOMNode;

    logfile : TDOMNode;

    j : Integer;

    entries : TStringList;

    s : string;

const  campaignFile = '\campaign file.xml';

begin
    try
        // Read in xml file from disk
        ReadXMLFile(Doc, cfg + campaignFile);
        entries := TStringList.Create;
        logfile := Doc.DocumentElement.FindNode('logFile');
        Child := logfile.FirstChild;

        while Assigned(Child)do
            begin
                // using ChildNodes method
                //with Child.ChildNodes do
                { replace the newlines with a carat (our delimiter) }
                s := Child.TextContent;
                s := StringReplace(s, #10, '^', [ rfReplaceAll ]);

                { make sure there is a trailing carat at the end of each string }
                if Copy(s, Length(s) - 1, 1) <> '^' then
                    s := s + '^';
                entries.Add(s);
                Child := Child.NextSibling;
            end;
    finally
        Doc.Free;
    end;
    readLogFile := entries;
end;

function writeLogFile( const cfg : string; const newEntry : string ) : boolean;
var
    Doc : TXMLDocument;

    Child : TDOMNode;
     TextNode  : TDOMNode;
    logfile : TDOMNode;
    j : Integer;

    s : string;

const  campaignFile = '\campaign file.xml';

begin
    try
        // Read in xml file from disk
        ReadXMLFile(Doc, cfg + campaignFile);
        logfile := Doc.DocumentElement.FindNode('logFile');
        Child :=Doc.CreateElement('logEntry');
        TextNode:=Doc.CreateCDATASection(newEntry);
        Child.AppendChild(TextNode);
        logfile.AppendChild(Child);
        writeXMLFile(Doc,cfg + campaignFile);
    finally
        Doc.Free;
    end;
    writeLogFile := true;
end;

function GetFileName( FullPath : string ) : string;
var
    StrFound : TStringList;

begin
    StrFound := TStringList.Create();
    ExtractStrings([ '\' ], [ ' ' ], PChar(FullPath), StrFound);
    result := StrFound[StrFound.Count - 1];
end;

function campaignFileExists( cfg : string ) : boolean;
const
    campaignFile = '\campaign file.xml';

begin
    if FileExists(cfg + campaignfile)then
        result := true
    else
        result := false;
end;

function makeCampaignFile( name : string; description : string; dest : string ) : boolean;
var
    check : boolean;

    cFile : TextFile;

begin
    AssignFile(cFile, dest + '\' + 'campaign file.xml');
    Rewrite(cFile);
    WriteLn(cFile,
        '<?xml version="1.0" encoding="UTF-8"?><root xsi:noNamespaceSchemaLocation = "XSaveGameManager.xsd" xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance">');
    WriteLn(cFile, '    <SGMversion>2</SGMversion>');
    WriteLn(cFile, '    <campaignName><![CDATA[' + name + ']]></campaignName>');
    WriteLn(cFile, '    <logEntryTemplate><![CDATA[');
    WriteLn(cFile, 'Log entry:');
    WriteLn(cFile, 'Day: ');
    WriteLn(cFile, ']]></logEntryTemplate>');
    WriteLn(cFile, '    <logFile>');
    WriteLn(cFile, '		<logEntry>');
    WriteLn(cFile, '		<![CDATA[');
    WriteLn(cFile,description);
    WriteLn(cFile, '           ]]>');
    WriteLn(cFile, '		</logEntry>');
    WriteLn(cFile, '	</logFile>');
    WriteLn(cFile, '</root>');
    CloseFile(cFile);
    makeCampaignFile := true;
end;
end.
