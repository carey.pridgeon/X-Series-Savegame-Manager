program sgm;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, main, lang, configOps, campaignOperations;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TmainGui, mainGui);
  Application.Run;
end.

