unit
    main;

{$mode objfpc}{$H+}
interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons, Menus, ExtCtrls, ComCtrls,
        CheckLst, FileCtrl, ShellCtrls, INIFiles, lang, campaignOperations,math;

type

    { TmainGui }

    TmainGui = class( TForm )
        AvailableCampaignsLabel : TLabel;
        activateCampaignButton : TButton;
        VaultEntryDescription: TLabel;
        vaultEntryDescriptionBox: TMemo;
        restoreFromVaultButton: TButton;
        DeleteFromVaultButton: TButton;
        vaultContents: TListBox;
        vaultAddButton: TButton;
        leaveVault: TButton;
        CampaignDescriptionLabel1: TLabel;
        campaignName: TEdit;
        createNewCancelButton: TButton;
        forkCancelButton: TButton;
        createForkedCampaign: TButton;
        forkedCampaignDescriptionLabel: TLabel;
        forkedampaignName: TEdit;
        createForkedCampaignPanel: TPanel;
        importOldCampaignsButton: TButton;
        ImportCampaignButton: TButton;
        ExportCampaignButton: TButton;
        gamePickerLabel: TLabel;
        infoMemo3: TMemo;
        infoMemo4: TMemo;
        forkCampaignDescription: TMemo;
        AddToVaultLabel: TLabel;
        availableCampaignsList: TListBox;
        viewVaultLabel: TLabel;
        vaultTitleLabel: TLabel;
        makeCampaignButton: TButton;
        newCampaignDescription: TMemo;
        newCampaignPanel: TPanel;
        ProgressBar: TProgressBar;
        SelectDirectoryDialog1: TSelectDirectoryDialog;
        SelectDirectoryDialog2: TSelectDirectoryDialog;
        SelectDirectoryDialog3: TSelectDirectoryDialog;
        typeCampaignName1: TLabel;
        typeForkedCampaignName: TLabel;
        UpdateStoredCampaignButton : TButton;
        insertTemplateButton : TButton;
        discardLogEntryButton : TButton;
        infoMemo2 : TMemo;
        saveLogEntryButton : TButton;
        CampaignDescriptionLabel : TLabel;
        onStartNewCancelButton : TButton;
        OnStartNewcampaignName : TEdit;
        editLogButton : TButton;
        forkCampaignButton : TButton;
        addLogEntryLabel : TLabel;
        onStartNewCampaignButton : TButton;
        campaignDescription : TMemo;
        infoMemo1 : TMemo;
        logEntryMemo : TMemo;
        onStartMakeCampaignPanel : TPanel;
        typeCampaignName : TLabel;
        vaultButton : TButton;
        dropBoxButton : TButton;
        CampaignsList : TListBox;
        createCampaignButton : TButton;
        currentlyActiveCampaignName : TEdit;
        currentlyActiveCampaignNameLabel : TLabel;
        gamePicker : TComboBox;
        LogMemo : TMemo;
        MainMenu1 : TMainMenu;
        MenuItem1 : TMenuItem;
        MenuItem2 : TMenuItem;
        Panel1 : TPanel;
        mainPanel : TPanel;
        logPanel : TPanel;
        dropboxPanel : TPanel;
        StatusBar : TStatusBar;
        UpDown : TUpDown;
        vaultPanel : TPanel;
        procedure activateCampaignButtonClick( Sender : TObject );
        procedure forkCancelButtonClick(Sender: TObject);
        procedure createCampaignButtonClick( Sender : TObject );
        procedure createForkedCampaignClick(Sender: TObject);
        procedure discardLogEntryButtonClick( Sender : TObject );
        procedure editLogButtonClick( Sender : TObject );
        procedure gamePickerLabelClick(Sender: TObject);
        procedure leaveVaultClick(Sender: TObject);
        procedure makeCampaignButtonClick(Sender: TObject);
        procedure onStartNewCancelButtonClick( Sender : TObject );
        procedure forkCampaignButtonClick( Sender : TObject );
        procedure insertTemplateButtonClick( Sender : TObject );
        procedure onStartNewCampaignButtonClick( Sender : TObject );
        procedure infoMemo1Change( Sender : TObject );
        procedure saveLogEntryButtonClick( Sender : TObject );
        procedure UpdateStoredCampaignButtonClick(Sender: TObject);
        procedure UpDownClick( Sender : TObject; Button : TUDBtnType );
        procedure vaultButtonClick( Sender : TObject );
        procedure dropBoxButtonClick( Sender : TObject );
        procedure ComboBox1Change( Sender : TObject );
        procedure FormCreate( Sender : TObject );
        procedure gamePickerChange( Sender : TObject );
        procedure Image1Click( Sender : TObject );
        procedure Label1Click( Sender : TObject );
        procedure MenuItem2Click( Sender : TObject );
        procedure Panel1Click( Sender : TObject );
        procedure mainPanelClick( Sender : TObject );

        private
        { private declarations }
        public
    { public declarations }
    end;

var
    mainGui : TmainGui;
    defaultLocationUsed : boolean;
    nonStandardCampaignLocation : string;
    statedCampaignLocation : string;
    statedVaultLocation : string;
    languageFile : string;
    userHomeDir : string;
    userDocuments : string;
    saveDir : string;
    gamePickerTextBackup : string;
    activeGame : Longint;
    activeGameStr : string;
    gameDirs : array [ 0..3 ] of string;
    campaigns : Tstringlist;
    vaultCampaigns : Tstringlist;
    pilotsLog : Tstringlist;
    INI : TINIFile;
    campaignMade : boolean;
    activateNoStorage : boolean;
    hasActiveCampaign : boolean;

const
    Egosoft = '\Egosoft';

    Save = '\save';

    x3 = 0;

    x3TC = 1;

    x3AP = 2;

    xR = 3;

implementation

uses
    XMLRead, XMLWrite, DOM;
{$R *.lfm}

{ TmainGui }

procedure TmainGui.activateCampaignButtonClick( Sender : TObject );
var
    check1 : boolean;
    check2 : boolean;
    check3 : boolean;
    dest : string;
    src : string;

begin
    if CampaignsList.ItemIndex > -1 then
        begin
            check1 := false;
            check2 := false;
            check3 := false;

            // store out the current campaign, and move the new campaign into place
            // first though, check to see if this is the currently active campaign already
            if ( CampaignsList.Items[CampaignsList.ItemIndex] = currentlyActiveCampaignName.caption ) then
                begin
                    Showmessage(getLangEntry('CannaBreakTheLawsOfPhysics'));
                end
            else
                begin
                    // not the same campaign, so we can do the funky copy and move thing.
                    // store the currently active campaign back in the campaigns folder
                    if activateNoStorage then
                        begin // enable all the buttons
                            UpdateStoredCampaignButton.Enabled := true;
                            createCampaignButton.Enabled := true;
                            forkCampaignButton.Enabled := true;
                            vaultButton.Enabled := true;
                            dropBoxButton.Enabled := true;
                            editLogButton.Enabled := true;
                        end
                    else
                        begin
                            StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderStart'
                                + currentlyActiveCampaignName.caption);
                            dest := statedCampaignLocation + gameDirs[activeGame]
                                + '\' + currentlyActiveCampaignName.caption;

                            if DirectoryExists(dest)then
                                begin
                                    check2 := CopyCampaign(activeGameStr, dest,ProgressBar);

                                    if not check2 then
                                        begin
                                            Showmessage(getLangEntry('errorOnCopy'));
                                            Close();
                                        end;
                                end;
                        end;

                    // active campaign copied out if needed, so activate the new one kthnxbye
                    src := statedCampaignLocation + gameDirs[activeGame]
                        + '\' + CampaignsList.items[CampaignsList.ItemIndex];
                    //showmessage(src);
                    activeGameStr := saveDir + gameDirs[activeGame] + Save;
                    StatusBar.SimpleText := getLangEntry('statusActivateCampaignStart'
                        + CampaignsList.items[CampaignsList.ItemIndex]);
                     Application.Processmessages;

                    check3 := CopyCampaign(src, activeGameStr,ProgressBar);

                    if check3 then
                        begin
                            currentlyActiveCampaignName.caption := getCampaignName(activeGameStr);
                            pilotsLog := readLogFile(activeGameStr);
                            LogMemo.Lines.Clear;
                            hasActiveCampaign := true;
                            if pilotsLog.count > 0 then
                                begin
                                    updown.min := 0;
                                    updown.max := pilotsLog.count - 1;
                                    updown.position := 0;
                                    LogMemo.Lines.Delimiter := '^';
                                    LogMemo.Lines.StrictDelimiter := True;
                                    LogMemo.Lines
                                        .DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
                                end;
                            StatusBar.SimpleText := getLangEntry('statusActivateCampaignFinish') + getCampaignName(activeGameStr);
                        end;
                end;
        end
    else
        begin
            Showmessage(getLangEntry('errorNoneSelected'));
            Close();
        end;
end;

procedure TmainGui.forkCancelButtonClick(Sender: TObject);
begin
    forkedampaignName.Caption := '';
    showmessage(getLangEntry('forkCancelMessage'));
    createForkedCampaignPanel.Top := 400;
    createForkedCampaignPanel.left := 400;
    mainPanel.Top := 0;
    mainPanel.left := 0;
end;

procedure TmainGui.createCampaignButtonClick( Sender : TObject );
begin
    onStartMakeCampaignPanel.Top := 0;
    onStartMakeCampaignPanel.left := 0;
    mainPanel.Top := 400;
    mainPanel.Left := 400;

end;

procedure TmainGui.createForkedCampaignClick(Sender: TObject);
    var
        x, y : integer;
        allowed : boolean;
        cnCheck : boolean;
        cCheck : boolean;
    begin
        allowed := true;

        // try to fork the campaign with the provided info, set campaignMade true on success
        if length(forkedampaignName.Caption) > 0 then
            begin
                //does this campaign already exist?
                for x := 0 to CampaignsList.Items.Count - 1 do
                    begin
                        if CampaignsList.items[x] = forkedampaignName.Caption then
                            begin
                                Showmessage(getLangEntry('campaignAlreadyExists'));
                                allowed := false;
                                break;
                            end;
                    end;

                if allowed then
                    begin
                        // to start with, copy out the current campaign
                         if not  IsDirectoryEmpty(activeGameStr) then begin
                           StatusBar.SimpleText := getLangEntry('newCampaignStoreCampaign');
                           cCheck := CopyCampaign(activeGameStr, statedCampaignLocation + gameDirs[ActiveGame]+ '\' + getCampaignName(activeGameStr),ProgressBar);
                           if not cCheck then begin
                               ShowMessage (getLangEntry('createNewCampaignPrvStoreError'));
                               Close();
                           end else begin
                               currentlyActiveCampaignName.caption := '';
                               LogMemo.lines.clear;
                               CampaignsList.Clear;
                               // clear the saves folder
                               //if not deleteFolderContent(activeGameStr) then showmessage('fail');
                           end;
                          end;



    // make the campaign, create a folder for it in campaigns, move a copy there and repopulate the campaigns list
                        makeCampaignFile(forkedampaignName.Caption, newCampaignDescription.Lines.Text, activeGameStr);
                        mkdir(statedCampaignLocation + gameDirs[activeGame] + '\' + forkedampaignName.Caption);
                        StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderStart') + getCampaignName(activeGameStr);

                        cnCheck := CopyCampaign(activeGameStr,
                            statedCampaignLocation + gameDirs[activeGame] + '\' + getCampaignName(activeGameStr),ProgressBar);

                        if cnCheck = true then
                            begin
                                StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderFinish') + getCampaignName(activeGameStr);

                                currentlyActiveCampaignName.caption := getCampaignName(activeGameStr);
                                pilotsLog := readLogFile(activeGameStr);
                                LogMemo.Lines.Clear;
                                hasActiveCampaign := true;
                                if pilotsLog.count > 0 then
                                    begin
                                        updown.min := 0;
                                        updown.max := pilotsLog.count - 1;
                                        updown.position := 0;
                                        LogMemo.Lines.Delimiter := '^';
                                        LogMemo.Lines.StrictDelimiter := True;
                                        LogMemo.Lines.DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
                                    end;
                                campaignMade := true;
                            end
                        else
                            begin
                                showmessage(getLangEntry('errorOnCopy'));
                                Close();
                            end;
                        // load and display the relevant campaigns folder
                        CampaignsList.Clear;
                        campaigns.clear;
                        GetSubDirectories(statedCampaignLocation + gameDirs[activeGame], campaigns);

                        if campaigns.count > 0 then
                            begin
                                for y := 0 to campaigns.Count - 1 do
                                    begin
                                        CampaignsList.items.Add(GetFileName(campaigns[y]));
                                    end;
                            end;
                    end;
                forkedampaignName.Caption := '';
                showmessage(getLangEntry('forkSuccess'));
                createForkedCampaignPanel.Top := 400;
                createForkedCampaignPanel.left := 400;
                mainPanel.Top := 0;
                mainPanel.left := 0;
            end
        else
            begin
                campaignMade := false;
                showmessage(getLangEntry('campaignNotNamed'));
                Close;
            end;
end;

procedure TmainGui.discardLogEntryButtonClick( Sender : TObject );
begin
    logPanel.Top := 400;
    logPanel.left := 400;
end;

procedure TmainGui.editLogButtonClick( Sender : TObject );
begin
    logPanel.Top := 0;
    logPanel.left := 0;
end;

procedure TmainGui.gamePickerLabelClick(Sender: TObject);
begin

end;

procedure TmainGui.leaveVaultClick(Sender: TObject);
begin
    mainPanel.Top := 0;
    mainPanel.Left := 0;
    vaultPanel.Top := 400;
    vaultPanel.Left := 400;

end;

procedure TmainGui.makeCampaignButtonClick(Sender: TObject);
var
    x, y : integer;
    allowed : boolean;
    cnCheck : boolean;
    cCheck : boolean;
begin
    allowed := true;

    // try to create the campaign with the provided info, set campaignMade true on success
    if length(campaignName.Caption) > 0 then
        begin
            //does this campaign already exist?
            for x := 0 to CampaignsList.Items.Count - 1 do
                begin
                    if CampaignsList.items[x] = OnStartNewcampaignName.Caption then
                        begin
                            Showmessage(getLangEntry('campaignAlreadyExists'));
                            allowed := false;
                            break;
                        end;
                end;

            if allowed then
                begin
                    // to start with, copy out the current campaign
                     if not  IsDirectoryEmpty(activeGameStr) then begin
                       StatusBar.SimpleText := getLangEntry('newCampaignStoreCampaign');
                       cCheck := CopyCampaign(activeGameStr, statedCampaignLocation + gameDirs[ActiveGame]+ '\' + getCampaignName(activeGameStr),ProgressBar);

                       if not cCheck then begin
                           ShowMessage (getLangEntry('createNewCampaignPrvStoreError'));
                           Close();
                       end else begin
                            showmessage('1');
                           currentlyActiveCampaignName.caption := '';
                           LogMemo.lines.clear;
                           CampaignsList.Clear;
                           // clear the saves folder
                           if not deleteFolderContent(activeGameStr) then showmessage('fail');
                       end;
                      end;


                   showmessage('here');


// make the campaign, create a folder for it in campaigns, move a copy there and repopulate the campaigns list
                    makeCampaignFile(campaignName.Caption, forkCampaignDescription.Text, activeGameStr);

                    mkdir(statedCampaignLocation + gameDirs[activeGame] + '\' + campaignName.Caption);
                    StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderStart') + getCampaignName(activeGameStr);

                    cnCheck := CopyCampaign(activeGameStr,
                        statedCampaignLocation + gameDirs[activeGame] + '\' + getCampaignName(activeGameStr),ProgressBar);

                    if cnCheck = true then
                        begin
                            StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderFinish') + getCampaignName(activeGameStr);

                            currentlyActiveCampaignName.caption := getCampaignName(activeGameStr);
                            pilotsLog := readLogFile(activeGameStr);
                            LogMemo.Lines.Clear;
                            hasActiveCampaign := true;
                            if pilotsLog.count > 0 then
                                begin
                                    updown.min := 0;
                                    updown.max := pilotsLog.count - 1;
                                    updown.position := 0;
                                    LogMemo.Lines.Delimiter := '^';
                                    LogMemo.Lines.StrictDelimiter := True;
                                    LogMemo.Lines.DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
                                end;
                            campaignMade := true;
                        end
                    else
                        begin
                            showmessage(getLangEntry('errorOnCopy'));
                            Close();
                        end;
                    // load and display the relevent campaigns folder
                    CampaignsList.Clear;
                    campaigns.clear;
                    GetSubDirectories(statedCampaignLocation + gameDirs[activeGame], campaigns);

                    if campaigns.count > 0 then
                        begin
                            for y := 0 to campaigns.Count - 1 do
                                begin
                                    CampaignsList.items.Add(GetFileName(campaigns[y]));
                                end;
                        end;
                end;
            campaignName.Caption := '';
            newCampaignPanel.Top := 400;
            newCampaignPanel.left := 400;
            vaultButton.enabled := true;
        end
    else
        begin
            campaignMade := false;
            showmessage(getLangEntry('campaignNotNamed'));
            Close;
        end;
end;

procedure TmainGui.onStartNewCancelButtonClick( Sender : TObject );
begin
    showmessage(getLangEntry('campaignNotNamed'));
    close();
end;

procedure TmainGui.forkCampaignButtonClick( Sender : TObject );
var
    tmp : string;

begin
    if not hasActiveCampaign  then begin
        Showmessage(getLangEntry('cannotFork'));
    end else begin
    createForkedCampaignPanel.Top := 0;
    createForkedCampaignPanel.Left := 0;
    mainPanel.Top := 400;
    mainPanel.Left := 400;

    end;
end;

procedure TmainGui.insertTemplateButtonClick( Sender : TObject );
var
    tmp : string;

begin
    if not ( activeGameStr = 'none' ) then
        begin
            logEntryMemo.Lines.Delimiter := '^';
            logEntryMemo.Lines.StrictDelimiter := True;
            tmp := getLogEntryTemplate(activeGameStr);
            logEntryMemo.Clear;
            logEntryMemo.Lines.DelimitedText := logEntryMemo.Lines.DelimitedText + tmp;
        end;
end;

procedure TmainGui.onStartNewCampaignButtonClick( Sender : TObject );
var
    x, y : integer;
    allowed : boolean;
    cnCheck : boolean;

begin
    allowed := true;

    // try to create the campaign with the provided info, set campaignMade true on success
    if length(OnStartNewcampaignName.Caption) > 0 then
        begin
            //does this campaign already exist?
            for x := 0 to CampaignsList.Items.Count - 1 do
                begin
                    if CampaignsList.items[x] = OnStartNewcampaignName.Caption then
                        begin
                            Showmessage(getLangEntry('campaignAlreadyExists'));
                            allowed := false;
                            break;
                        end;
                end;

            if allowed then
                begin
// make the campaign, create a folder for it in campaigns, move a copy there and repopulate the campaigns list
                    makeCampaignFile(OnStartNewcampaignName.Caption, campaignDescription.Lines.Text, activeGameStr);
                    mkdir(statedCampaignLocation + gameDirs[activeGame] + '\' + OnStartNewcampaignName.Caption);
                    StatusBar
                        .
                            SimpleText := getLangEntry('statusCopyToCampaignsFolderStart') + getCampaignName(activeGameStr);
                    cnCheck := CopyCampaign(activeGameStr,
                        statedCampaignLocation + gameDirs[activeGame] + '\' + getCampaignName(activeGameStr),ProgressBar);

                    if cnCheck = true then
                        begin
                            StatusBar
                                .
                                    SimpleText := getLangEntry('statusCopyToCampaignsFolderFinish') + getCampaignName(activeGameStr);
                            currentlyActiveCampaignName.caption := getCampaignName(activeGameStr);
                            pilotsLog := readLogFile(activeGameStr);
                            LogMemo.Lines.Clear;
                            hasActiveCampaign := true;
                            if pilotsLog.count > 0 then
                                begin
                                    updown.min := 0;
                                    updown.max := pilotsLog.count - 1;
                                    updown.position := 0;
                                    LogMemo.Lines.Delimiter := '^';
                                    LogMemo.Lines.StrictDelimiter := True;
                                    LogMemo.Lines.DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
                                end;
                            campaignMade := true;
                        end
                    else
                        begin
                            showmessage(getLangEntry('errorOnCopy'));
                            Close();
                        end;
                    // load and display the relevent campaigns folder
                    CampaignsList.Clear;
                    campaigns.clear;
                    GetSubDirectories(statedCampaignLocation + gameDirs[activeGame], campaigns);

                    if campaigns.count > 0 then
                        begin
                            for y := 0 to campaigns.Count - 1 do
                                begin
                                    CampaignsList.items.Add(GetFileName(campaigns[y]));
                                end;
                        end;
                end;
            OnStartNewcampaignName.Caption := '';
            onStartMakeCampaignPanel.Top := 400;
            onStartMakeCampaignPanel.left := 400;
            mainPanel.Top := 0;
            mainPanel.Left := 0;
        end
    else
        begin
            campaignMade := false;
            showmessage(getLangEntry('campaignNotNamed'));
            Close;
        end;
end;

procedure TmainGui.infoMemo1Change( Sender : TObject );
begin
end;

procedure TmainGui.saveLogEntryButtonClick( Sender : TObject );
begin
  writeLogFile(activeGameStr,logEntryMemo.Lines.Text);
  pilotsLog := readLogFile(activeGameStr);
  if pilotsLog.count > 0 then
   begin
   updown.min := 0;
   updown.max := pilotsLog.count;
   updown.position := updown.max-1;
   LogMemo.Lines.clear;
   LogMemo.Lines.Delimiter := '^';
   LogMemo.Lines.StrictDelimiter := True;
   LogMemo.Lines.DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
   end;
    // finished, so move the log entry panel back out  of view
    logPanel.Top := 400;
    logPanel.left := 400;
end;

procedure TmainGui.UpdateStoredCampaignButtonClick(Sender: TObject);
var
    cCheck : boolean;
begin
    if (not ActiveGame =-1) then begin
       if not  IsDirectoryEmpty(activeGameStr) then begin
            StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderStart') + getCampaignName(activeGameStr);
             cCheck := CopyCampaign(activeGameStr, statedCampaignLocation + gameDirs[ActiveGame]+ '\' + getCampaignName(activeGameStr),ProgressBar);

              if not cCheck then begin
               showmessage(getLangEntry('errorOnCopy'));
                Close();
                end;
        StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderFinish') + getCampaignName(activeGameStr);
       end;

  end else begin
        showmessage(getLangEntry('NoGameSelected'));
      end;
end;

procedure TmainGui.UpDownClick( Sender : TObject; Button : TUDBtnType );
begin
    if (pilotsLog.count > 0) and (updown.position < pilotsLog.count) then
        begin
            LogMemo.clear;
            LogMemo.Lines.Delimiter := '^';
            LogMemo.Lines.StrictDelimiter := True;
            LogMemo.Lines.DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
        end;
end;

procedure TmainGui.vaultButtonClick( Sender : TObject );
var
    y : integer;
begin
    vaultPanel.Top := 0;
    vaultPanel.Left := 0;
    mainPanel.Top := 400;
    mainPanel.Left := 400;
    // load and display the relevent campaigns folder
     availableCampaignsList.Clear;
     campaigns.clear;
     GetSubDirectories(statedCampaignLocation + gameDirs[activeGame], campaigns);
     if campaigns.count > 0 then
        begin
           for y := 0 to campaigns.Count - 1 do
               begin
                    availableCampaignsList.items.Add(GetFileName(campaigns[y]));
               end;
     end;
     vaultCampaigns.clear;
     GetSubDirectories(statedVaultLocation + gameDirs[activeGame], vaultCampaigns);
     if vaultCampaigns.count > 0 then
        begin
           for y := 0 to vaultCampaigns.Count - 1 do
               begin
                    vaultContents.items.Add(GetFileName(vaultCampaigns[y]));
               end;
     end;
end;

procedure TmainGui.dropBoxButtonClick( Sender : TObject );

begin

end;

procedure TmainGui.ComboBox1Change( Sender : TObject );
begin
end;

procedure TmainGui.FormCreate( Sender : TObject );
var
    j, x, y : integer;
    contents : TStringlist;
    tmp : string;
    pCheck : boolean;
    cCheck : boolean;
    allowed : boolean;
    cnCheck : boolean;
    iniCheck : boolean;

begin
    pilotsLog := tStringlist.create;
    campaigns := TStringList.Create;
    vaultCampaigns := TStringList.Create;
    pCheck := false;
    cCheck := false;
    iniCheck := false;
    mainGui.Height := 410;
    mainGui.Width := 768;
    campaignMade := false;
    activateNoStorage := false;
    hasActiveCampaign := false;
    updown.min := 0;
    updown.max := 0;
    updown.position := 0;
    // setup save location folder arrays
    gameDirs[3] := '\X3';
    gameDirs[2] := '\X3AP';
    gameDirs[1] := '\X3TC';
    gameDirs[0] := '\XR';
    // ini file try
    INI := TINIFile.Create('cfg.ini');
    statedCampaignLocation := INI.ReadString('Default', 'campaignLocation', '');
    statedVaultLocation := INI.ReadString('Default', 'vaultLocation', '');
    LanguageFile := INI.ReadString('Default', 'language', '');

    if (statedCampaignLocation = '') or (statedVaultLocation = '') then
        begin
            inicheck := true;
            INI.WriteString('Default', 'campaignLocation', 'here');
            INI.WriteString('Default', 'vaultLocation', 'here');
            INI.WriteString('Default', 'language', 'en');
            INI.WriteInteger('Default', 'activeGame', -1);
            statedCampaignLocation := INI.ReadString('Default', 'campaignLocation', '');
            statedVaultLocation := INI.ReadString('Default', 'vaultLocation', '');
            languageFile := INI.ReadString('Default', 'language', '');
            activeGame := INI.ReadInteger('Default', 'activeGame', -1);
        end;

    if LanguageFile = '' then
        begin
            iniCheck := true;
            INI.WriteString('Default', 'campaignLocation', 'here');
            INI.WriteString('Default', 'vaultLocation', 'here');
            INI.WriteString('Default', 'language', 'en');
            statedCampaignLocation := INI.ReadString('Default', 'campaignLocation', '');
            statedVaultLocation := INI.ReadString('Default', 'vaultLocation', '');
            languageFile := INI.ReadString('Default', 'language', '');

        end;

    // time to either open and scan the campaigns folder or create it if it doesn't exist
    if statedCampaignLocation = 'here' then
        begin // the campaigns folder is in it's normal place
            statedCampaignLocation := 'Campaigns';
            activeGame := INI.ReadInteger('Default', 'activeGame', -1);
        end;

    if not DirectoryExists(statedCampaignLocation)then
        begin
            CreateDir(statedCampaignLocation);
            CreateDir(statedCampaignLocation + gameDirs[0]);
            CreateDir(statedCampaignLocation + gameDirs[1]);
            CreateDir(statedCampaignLocation + gameDirs[2]);
            CreateDir(statedCampaignLocation + gameDirs[3]);
        end;

     // same for the vault location
    if statedVaultLocation = 'here' then
        begin // the vault folder is in it's normal place
            statedVaultLocation := 'Vault';
            activeGame := INI.ReadInteger('Default', 'activeGame', -1);
        end;

    if not DirectoryExists(statedVaultLocation)then
        begin
            CreateDir(statedVaultLocation);
            CreateDir(statedVaultLocation + gameDirs[0]);
            CreateDir(statedVaultLocation + gameDirs[1]);
            CreateDir(statedVaultLocation + gameDirs[2]);
            CreateDir(statedVaultLocation + gameDirs[3]);
        end;

    //  get mydocuments location
    userDocuments := GetMyDocuments();
    userHomeDir := GetUserDir();
    saveDir := userDocuments + Egosoft;
    LanguageFile := makeLangFilename(LanguageFile);

    if ( checkLangFileExists(LanguageFile)) then
        begin
            if ( loadLangFile(LanguageFile)) then
                begin
                    // first say if we had to regenerate the ini file
                    if iniCheck then
                        StatusBar.simpletext :=getLangEntry('makeIniFile');
                    // set up the game picker drop down box
                    gamePicker.Text := getLangEntry('gamePickerText');
                    gamePickerTextBackup := gamePicker.Text;

                    for j := 0 to gamePicker.Items.count - 1 do
                        begin
                            gamePicker.Items[j] := getLangEntry(gamePicker.Items[j]);
                        end;
                    gamePicker.Items.Add(gamePickerTextBackup);
                    // set up various buttons and labels
                    mainGui.Caption := getLangEntry(mainGui.Caption);
                    mainGui.gamePickerLabel.Caption :=getLangEntry(mainGui.gamePickerLabel.Caption);
                    currentlyActiveCampaignNameLabel.Caption := getLangEntry(currentlyActiveCampaignNameLabel.Caption);
                    currentlyActiveCampaignName.Text := getLangEntry(currentlyActiveCampaignName.Text);
                    AvailableCampaignsLabel.Caption := getLangEntry(AvailableCampaignsLabel.Caption);
                    activateCampaignButton.Caption := getLangEntry(activateCampaignButton.Caption);
                    createCampaignButton.Caption := getLangEntry(createCampaignButton.Caption);
                    forkCampaignButton.Caption := getLangEntry(forkCampaignButton.Caption);
                    vaultButton.Caption := getLangEntry(vaultButton.Caption);
                    dropBoxButton.Caption := getLangEntry(dropBoxButton.Caption);
                    editLogButton.Caption := getLangEntry(editLogButton.Caption);
                    typeCampaignName.caption := getLangEntry(typeCampaignName.caption);
                    CampaignDescriptionLabel.caption := getLangEntry(CampaignDescriptionLabel.caption);
                    onStartNewCancelButton.caption := getLangEntry(onStartNewCancelButton.caption);
                    createNewCancelButton.caption := getLangEntry(createNewCancelButton.caption);
                    onStartNewCampaignButton.caption := getLangEntry(onStartNewCampaignButton.caption);
                    addLogEntryLabel.caption := getLangEntry(addLogEntryLabel.caption);
                    saveLogEntryButton.caption := getLangEntry(saveLogEntryButton.caption);
                    discardLogEntryButton.caption := getLangEntry(discardLogEntryButton.caption);
                    insertTemplateButton.caption := getLangEntry(insertTemplateButton.caption);
                    UpdateStoredCampaignButton.caption := getLangEntry(UpdateStoredCampaignButton.caption);
                    forkCancelButton.caption := getLangEntry(forkCancelButton.caption);
                    createForkedCampaign.caption := getLangEntry(createForkedCampaign.caption);
                    forkedCampaignDescriptionLabel.caption := getLangEntry(forkedCampaignDescriptionLabel.caption);
                    typeForkedCampaignName.caption := getLangEntry(typeForkedCampaignName.caption);
                    vaultTitleLabel.caption := getLangEntry(vaultTitleLabel.caption);
                    AddToVaultLabel.caption := getLangEntry(AddToVaultLabel.caption);
                    viewVaultLabel.caption := getLangEntry(viewVaultLabel.caption);
                    leaveVault.caption := getLangEntry(leaveVault.caption);
                    vaultAddButton.caption := getLangEntry(vaultAddButton.caption);
                    restoreFromVaultButton.caption := getLangEntry(restoreFromVaultButton.caption);
                    DeleteFromVaultButton.caption := getLangEntry(DeleteFromVaultButton.caption);
                    VaultEntryDescription.caption := getLangEntry(VaultEntryDescription.caption);
                    infoMemo1.Lines.Delimiter := '^';
                    infoMemo1.Lines.StrictDelimiter := True;
                    tmp := getLangEntry(infoMemo1.lines[0]);
                    infoMemo1.Clear;
                    infoMemo1.Lines.DelimitedText := infoMemo1.Lines.DelimitedText + tmp;
                    infoMemo2.Lines.Delimiter := '^';
                    infoMemo2.Lines.StrictDelimiter := True;
                    tmp := getLangEntry(infoMemo2.lines[0]);
                    infoMemo2.Clear;
                    infoMemo2.Lines.DelimitedText := infoMemo2.Lines.DelimitedText + tmp;
                    infoMemo3.Lines.Delimiter := '^';
                    infoMemo3.Lines.StrictDelimiter := True;
                    tmp := getLangEntry(infoMemo3.lines[0]);
                    infoMemo3.Clear;
                    infoMemo3.Lines.DelimitedText := infoMemo3.Lines.DelimitedText + tmp;
                    tmp := getLangEntry(infoMemo4.Lines[0]);
                    infoMemo4.Lines.Delimiter := '^';
                    infoMemo4.Lines.StrictDelimiter := True;
                    infoMemo4.Lines.Clear;
                    infoMemo4.Lines.DelimitedText := infoMemo4.Lines.DelimitedText + tmp;
                end
            else
                begin
                    ShowMessage('Language file damaged or missing, exiting');
                    halt();
                end;
        end;
    // standard startup checks, are there any savegames in the required folder
    contents := TStringlist.create;
    gamePicker.ItemIndex := activeGame;

    if ( gamePicker.ItemIndex > -1 ) and (gamePicker.ItemIndex < 4) then
        begin
            activeGameStr := saveDir + gameDirs[activeGame] + Save;
            // load and display the relevent campaigns folder
            CampaignsList.Clear;
            campaigns.clear;
            GetSubDirectories(statedCampaignLocation + gameDirs[activeGame], campaigns);

            if campaigns.count > 0 then
                begin
                    for x := 0 to campaigns.Count - 1 do
                        begin
                            CampaignsList.items.Add(GetFileName(campaigns[x]));
                        end;
                end;

            // check to see if there are any actual saves (simple check, is the directory empty)
            if IsDirectoryEmpty(activeGameStr)then
                begin
                    if campaigns.count = 0 then
                        begin
                            ShowMessage(getLangEntry('errorNoSavesOrCampaign'));
                            Close;
                        end;
                    ShowMessage(getLangEntry('errorPickCampaign'));
// the activation needs to be done without trying to store the current campaign, since there isn't one.
                    activateNoStorage := true;
                    // and we want to disable all other buttons until one has been activated
                    UpdateStoredCampaignButton.Enabled := false;
                    activateCampaignButton.Enabled := false;
                    createCampaignButton.Enabled := false;
                    forkCampaignButton.Enabled := false;
                    vaultButton.Enabled := false;
                    dropBoxButton.Enabled := false;
                    editLogButton.Enabled := false;
                end
            else
                begin
                    // check to see if the campaign has been backed up into the Campaigns folder

                    if campaignFileExists(activeGameStr)then
                        begin
                            // check to see if the campaign has been backed up into the Campaigns folder yet
                            if DoesDirectoryExist(statedCampaignLocation + gameDirs[activeGame]+ '\' + getCampaignName(activeGameStr))then
                                pcheck := true;

                            if not pCheck then
                                begin // we need to store a copy of this campaign in the Campaigns folder
                                    StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderStart') + getCampaignName(activeGameStr);
                                    mkdir(statedCampaignLocation + gameDirs[activeGame]
                                                + '\' + getCampaignName(activeGameStr));
                                    cCheck := CopyCampaign(activeGameStr, statedCampaignLocation + gameDirs[activeGame]
                                        + '\' + getCampaignName(activeGameStr),ProgressBar);

                                    if cCheck = true then
                                        begin
                                            StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderFinish') + getCampaignName(activeGameStr);
                                        end
                                    else
                                        begin
                                            showmessage(getLangEntry('errorOnCopy'));
                                            Close();
                                        end;
                                end;
                            // ok, it looks like we have an active campaign and it's stored now as well, so proceed
                            currentlyActiveCampaignName.caption := getCampaignName(activeGameStr);
                            vaultButton.enabled := true;
                            // display the log file
                            pilotsLog := readLogFile(activeGameStr);
                            hasActiveCampaign := true;
                            if pilotsLog.count > 0 then
                                begin
                                    updown.min := 0;
                                    updown.max := pilotsLog.count;
                                    updown.position := updown.max-1;
                                    LogMemo.Lines.Delimiter := '^';
                                    LogMemo.Lines.StrictDelimiter := True;
                                    LogMemo.Lines.DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
                                end;
                        end
                    else
                        begin // there are saves, but they aren't in a campaign, so let's do that
                            ShowMessage(getLangEntry('newCampaignCreationRequired'));
                            onStartMakeCampaignPanel.Top := 0;
                            onStartMakeCampaignPanel.left := 0;
                        end;
                end;
        end
    else
        begin
        // there is no game selected
        StatusBar.SimpleText := getLangEntry('pickAGame');
        UpdateStoredCampaignButton.Enabled := false;
        activateCampaignButton.Enabled := false;
        createCampaignButton.Enabled := false;
        forkCampaignButton.Enabled := false;
        vaultButton.Enabled := false;
        dropBoxButton.Enabled := false;
        editLogButton.Enabled := false;
        end;
end;

procedure TmainGui.gamePickerChange( Sender : TObject );
var
    present : boolean;
    x : integer;
    pCheck : boolean;
    cCheck : boolean;
    prvActiveGame : integer;
begin
    if gamePicker.ItemIndex < 4 then
        begin
            pCheck := false;
            INI.WriteInteger('Default', 'activeGame', gamePicker.ItemIndex);
            prvActiveGame := activeGame;
            if (prvActiveGame >-1) and (prvActiveGame<4) then begin
              // the previous games campaign needs to have its stored copy updated.
                if not  IsDirectoryEmpty(activeGameStr) then begin
                   StatusBar.SimpleText := getLangEntry('changeGameStoreCampaign');
                   cCheck := CopyCampaign(activeGameStr, statedCampaignLocation + gameDirs[prvActiveGame]+ '\' + getCampaignName(activeGameStr),ProgressBar);

                   if not cCheck then begin
                       ShowMessage (getLangEntry('changeGameStoreCampaignError'));
                       Close();
                   end else begin
                       currentlyActiveCampaignName.caption := '';
                       LogMemo.lines.clear;
                       CampaignsList.Clear;
                   end;
              end;

            end;
            activeGame := gamePicker.ItemIndex;
            // build the game folder string
            activeGameStr := saveDir + gameDirs[activeGame] + Save;
            // does the player have the game in question?
            present := DoesDirectoryExist(activeGameStr);
            if present then
                begin
                    present := DoesDirectoryExist(activeGameStr);

                    if present then
                        begin
                            //various things to do
                            // load and display the relevent campaigns folder
                            UpdateStoredCampaignButton.Enabled := true;
                            activateCampaignButton.Enabled := true;
                            createCampaignButton.Enabled := true;
                            forkCampaignButton.Enabled := true;
                            vaultButton.Enabled := true;
                            dropBoxButton.Enabled := true;
                            editLogButton.Enabled := true;
                            CampaignsList.Clear;
                            campaigns.Clear;
                            GetSubDirectories(statedCampaignLocation + gameDirs[activeGame], campaigns);

                            if campaigns.count > 0 then
                                begin
                                    for x := 0 to campaigns.Count - 1 do
                                        begin
                                            CampaignsList.items.Add(GetFileName(campaigns[x]));
                                        end;

                                    // check to see if there are any actual saves
                                    if IsDirectoryEmpty(activeGameStr)then begin
                                        ShowMessage(getLangEntry('errorPickCampaign'));
                                        activateNoStorage := true;
                                        // and we want to disable all other buttons until one has been activated
                                        UpdateStoredCampaignButton.Enabled := false;
                                        createCampaignButton.Enabled := false;
                                        forkCampaignButton.Enabled := false;
                                        vaultButton.Enabled := false;
                                        dropBoxButton.Enabled := false;
                                        editLogButton.Enabled := false;
                                    end else if campaignFileExists(activeGameStr)then
                                        begin
                                            currentlyActiveCampaignName.caption := getCampaignName(activeGameStr);
                                            UpdateStoredCampaignButton.Enabled := true;
                                            activateCampaignButton.Enabled := true;
                                            createCampaignButton.Enabled := true;
                                            forkCampaignButton.Enabled := true;
                                            vaultButton.Enabled := true;
                                            dropBoxButton.Enabled := true;
                                            editLogButton.Enabled := true;
                                            pilotsLog := readLogFile(activeGameStr);
                                            LogMemo.clear;

                                            if pilotsLog.count > 0 then
                                                begin
                                                    updown.min := 0;
                                                    updown.max := pilotsLog.count - 1;
                                                    updown.position := 0;
                                                    LogMemo.Lines.Delimiter := '^';
                                                    LogMemo.Lines.StrictDelimiter := True;
                                                    LogMemo.Lines.DelimitedText := LogMemo.Lines.DelimitedText + pilotsLog[updown.position];
                                                end;
                                            // now has it been stored yet? If not, do that now
                                             // check to see if the campaign has been backed up into the Campaigns folder yet
                                              if DoesDirectoryExist(statedCampaignLocation + gameDirs[activeGame]
                                                  + '\' + getCampaignName(activeGameStr))then
                                                  pcheck := true;

                                              if not pCheck then
                                                  begin // we need to store a copy of this campaign in the Campaigns folder
                                                      mkdir(statedCampaignLocation + gameDirs[activeGame]+ '\' + getCampaignName(activeGameStr));
                                                      StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderStart') + getCampaignName(activeGameStr);
                                                      cCheck := CopyCampaign(activeGameStr, statedCampaignLocation + gameDirs[activeGame]+ '\' + getCampaignName(activeGameStr),ProgressBar);

                                                      if cCheck = true then
                                                          begin
                                                              StatusBar.SimpleText := getLangEntry('statusCopyToCampaignsFolderFinish') + getCampaignName(activeGameStr);
                                                              // reload and display the relevent campaigns folder
                                                              CampaignsList.Clear;
                                                              campaigns.clear;
                                                              GetSubDirectories(statedCampaignLocation + gameDirs[activeGame], campaigns);

                                                              if campaigns.count > 0 then
                                                                  begin
                                                                      for x := 0 to campaigns.Count - 1 do
                                                                          begin
                                                                              CampaignsList.items.Add(GetFileName(campaigns[x]));
                                                                          end;
                                                                  end;
                                                          end
                                                      else
                                                          begin
                                                              showmessage(getLangEntry('errorOnCopy'));
                                                              Close();
                                                          end;
                                                  end;
                                        end else begin
                                            // there are saves, they need to be made into a campaign
                                           ShowMessage(getLangEntry('newCampaignCreationRequired'));
                                           onStartMakeCampaignPanel.Top := 0;
                                           onStartMakeCampaignPanel.left := 0;
                                        end;
                                end
                            else
                                ShowMessage(getLangEntry('errorNoCampaignsFound'));
                        end
                    else
                        begin
                            ShowMessage(getLangEntry('errorNoSaveFolderFound'));
                            activeGame := -1;
                            gamePicker.ItemIndex := 4;
                            activeGameStr := 'none';
                            INI := TINIFile.Create('cfg.ini');
                            INI.WriteInteger('Default', 'activeGame', -1);
                            UpdateStoredCampaignButton.Enabled := false;
                            activateCampaignButton.Enabled := false;
                            createCampaignButton.Enabled := false;
                            forkCampaignButton.Enabled := false;
                            vaultButton.Enabled := false;
                            dropBoxButton.Enabled := false;
                            editLogButton.Enabled := false;
                        end;
                end
            else
                begin
                    Case activeGame of
                        0 : ShowMessage(getLangEntry('ns1'));
                        1 : ShowMessage(getLangEntry('ns2'));
                        2 : ShowMessage(getLangEntry('ns3'));
                        3 : ShowMessage(getLangEntry('ns4'));
                    end;
                    activeGame := -1;
                    INI := TINIFile.Create('cfg.ini');
                    INI.WriteInteger('Default', 'activeGame', -1);
                    gamePicker.ItemIndex := 4;
                    UpdateStoredCampaignButton.Enabled := false;
                    activateCampaignButton.Enabled := false;
                    createCampaignButton.Enabled := false;
                    forkCampaignButton.Enabled := false;
                    vaultButton.Enabled := false;
                    dropBoxButton.Enabled := false;
                    editLogButton.Enabled := false;
                end;
        end;
end;

procedure TmainGui.Image1Click( Sender : TObject );
begin
end;

procedure TmainGui.Label1Click( Sender : TObject );
begin
end;

procedure TmainGui.MenuItem2Click( Sender : TObject );
begin
    Close();
end;

procedure TmainGui.Panel1Click( Sender : TObject );
begin
end;

procedure TmainGui.mainPanelClick( Sender : TObject );
begin
end;
end.
